public class Funcionario{

	private String nome;
	private String departamento;
	private double salario;
	private String rg;
	private boolean permanece;

	
	public void setNome(String nome){
		this.nome = nome;	
	}
	public String getNome(){
		return nome;
	}
	public void setDepartamento(String departamento){
		this.departamento = departamento;
	}	
	public String getDepartamento(){
		return departamento;
	}
	public void setSalario(double salario){
		this.salario = salario;	
	}
	public double getSalario(){
		return salario;
	}
	public void setRg(String rg){
		this.rg = rg;
	}
	public String getRg(){
		return rg;
	}

	public void setPermanece(boolean permanece){
		this.permanece = permanece;
	}
	
	
	
	public void bonifica(double valorAumento){
		this.salario = this.salario + valorAumento;
	}
	
	public void mostra(){
		System.out.println("Nome: "+this.nome);
		System.out.println("Departamento: "+this.departamento);
		System.out.println("Salario: "+this.salario);
		System.out.println("RG: "+this.rg);
		if(this.permanece == true)
			System.out.println("O funcionario permanece na empresa.");
		else{
			System.out.println("O funcionario nao trabalha mais na empresa.");
		}
	}
}
