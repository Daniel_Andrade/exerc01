import java.util.Scanner;

public class Teste {
	public static void main(String[] args){
		Scanner ler = new Scanner(System.in);
		Funcionario f1 = new Funcionario();
		Data data = new Data();
		
		data.dia = 21;
		data.mes = 04;
		data.ano = 2013;
		
		System.out.println("Nome1: ");
		f1.setNome(ler.nextLine());
		System.out.println("Departamento1: ");
		f1.setDepartamento(ler.nextLine());
		System.out.println("Salario1: ");
		f1.setSalario(ler.nextDouble());
		System.out.println("RG1: ");
		f1.setRg(ler.next());
		System.out.println("O funcionario ainda trabalha na empresa? \nTrue\nFalse");
		f1.setPermanece(ler.nextBoolean());
		
		Funcionario f2 = new Funcionario();
		
		System.out.println("Nome2: ");
		f2.setNome(ler.next());
		System.out.println("Departamento2: ");
		f2.setDepartamento(ler.next());
		System.out.println("Salario2: ");
		f2.setSalario(ler.nextDouble());
		System.out.println("RG2: ");
		f2.setRg(ler.next());
		System.out.println("O funcionario ainda trabalha na empresa? \nTrue ou False");
		f2.setPermanece(ler.nextBoolean());
		ler.close();
		
		if(f1 == f2)
			System.out.println("Iguais");
		else{
			System.out.println("Diferentes");
		}
		f1.mostra();
		System.out.println("\n");
		f2.mostra();
		System.out.println(data.toString() + "\n");
		
	}
}
